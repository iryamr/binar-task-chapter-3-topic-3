package id.binar.learn.tasktopic3

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import id.binar.learn.tasktopic3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setViewPager()

        binding.bottomNavView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.first_fragment -> binding.viewPager.currentItem = 0
                R.id.second_fragment -> binding.viewPager.currentItem = 1
            }
            true
        }
    }

    private fun setViewPager() {
        val listFragment = mutableListOf(
            FirstFragment(),
            SecondFragment()
        )

        val viewPagerAdapter = ViewPagerAdapter(listFragment, supportFragmentManager, lifecycle)
        binding.viewPager.adapter = viewPagerAdapter

        val callback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                binding.bottomNavView.menu.getItem(position).isChecked = true
            }
        }

        binding.viewPager.registerOnPageChangeCallback(callback)
    }
}